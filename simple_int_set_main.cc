// simple_int_set_main.cc
#include "simple_int_set.h"
#include <iostream>
#include <string>
#include <stdlib.h>
#include <stdio.h>
#define MAX_SIZE 100

void sortInt(int * array , int num)
{
	int tmp = 0;
	for (int start = 1 ; start < num ; start ++)
		for (int i = start ; i < num ; i ++)
			if (array[i] < array[start - 1])
			{
				tmp = array[start - 1];
				array[start - 1] = array[i];
				array [i] = tmp;
			}
}

using namespace std;

int main(void){
	while (true)
	{
	int check = 1;
	char input=' ',k=' ',op='a';
	SimpleIntSet first, second, result;
	int errer = 0,int_array[MAX_SIZE],tmp=0,num = 0;
	
	if ((scanf("%c",&k)) == 0 || k != '{')
		return 1;
	while (1){
		if ((errer = scanf("%d", &tmp)) == 0)
			break;
		for (int i = 0 ; i < num ; i ++){
			if (int_array[i] == tmp)
				check = 0;
		}
		if (check){
			int_array[num] = tmp;
			num ++;
		}
		check = 1;
		
	}
	scanf("%c",&k);
	if (k == '}'){
		sortInt(int_array, num);
		first.Set(int_array, num);
	}
	else
		return 1;
	num = 0;
	scanf(" %c", &op);
	
	if (op != '+' && op != '*' && op!='-')
		return 1;

	if ((scanf(" %c",&k)) == 0 || k != '{')
		return 1;

	while (1){
		if ((errer = scanf("%d", &tmp)) == 0)
			break;
		for (int i = 0 ; i < num ; i ++){
			if (int_array[i] == tmp)
				check = 0;
		}
		if (check){
			int_array[num] = tmp;
			num ++;
		}
		check = 1;
	}

	scanf("%c",&k);
	
	if (k == '}'){
		sortInt(int_array, num);
		second.Set(int_array, num);
	}
	else
		return 1;
	
	switch (op){
		case '+' :
			result = first.Union(second);
			break;
		case '-' :
			result = first.Difference(second);
			break;
		case '*' :
			result = first.Intersect(second);
			break;
	}
	cout << "{" ;
	for (int i = 0 ; i < result.size() ; i ++)
	{
		cout << " " << result.values()[i];
	}
	cout << " }" << endl;
  	scanf("%c",&k);
	}
}
