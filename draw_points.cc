// draw_points.cc

#include <iostream>
using namespace std;
struct Point {
  unsigned int x, y;
};

// Implement this function.
void DrawPoints(const Point* points, int num_points);

int main() {
// Fill the main function.
	while(true)
	{
		int num = 0, raw = 0, col = 0;
		cin >> num;
		if (num == 0)
			return 1;
		Point pointers[num];
		for (int i = 0 ; i < num ; i ++)
		{
			cin >> pointers[i].x >> pointers[i].y;
		}
		DrawPoints(pointers, num);
		
	}
}
void DrawPoints(const Point* points, int num_points)
{
	int col = 0, raw = 0;
	for (int i = 0 ; i < num_points ; i ++)
	{
		if (col < points[i].x)
			col = points[i].x;
		if (raw < points[i].y) 
			raw = points[i].y;
	}
//	cout << "col = " << col << "raw = " << raw << endl;
	col ++;
	raw ++;
//	cout <<"col2 = "<< col <<"raw2 = " << raw << endl;
	
	char stars[col*raw];
	for (int i = 0 ; i < col * raw ; i ++)
		stars[i] = '.';
	for (int i = 0 ; i < num_points ; i ++)
	{
		stars[points[i].x + points[i].y*col] = '*';
	}
	for (int i = 0 ; i < raw ; i ++)
	{
		for (int k = 0 ; k < col ; k ++)
			cout << stars[k+i*col];
		cout << endl;
	}
	
}

