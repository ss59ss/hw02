# Makefile

#all: radix_notation draw_points simple_calculator simple_int_set



make : radix_notation.cc draw_points.cc simple_calculator.cc simple_calculator.h simple_calculator_main.cc simple_int_set.cc simple_int_set.h simple_int_set_main.cc
	g++ -o radix_notation radix_notation.cc
	g++ -o draw_points draw_points.cc
	g++ -o simple_calculator simple_calculator.cc simple_calculator.h simple_calculator_main.cc
	g++ -o simple_int_set simple_int_set.cc simple_int_set.h simple_int_set_main.cc	
