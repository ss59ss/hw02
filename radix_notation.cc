// radix_notation.cc

#include <string>
#include <iostream>

using namespace std;

// Implement this function.
string RadixNotation(unsigned int number, unsigned int radix);

int main() {
  while (true) {
    unsigned int number, radix;
   
    cin >> number >> radix;
    if (radix < 2 || radix > 36) 
		break;

    cout << RadixNotation(number, radix) << endl;

  }
  return 0;
}

string RadixNotation(unsigned int number, unsigned int radix)
{
  int i=0,rest = 0;
  char tmp[5]="";
  string reverse="", result="";
  
  while (number >= radix)
  {
    rest = number % radix;
    number /= radix;
    if (rest >= 10)
    {

	rest += 7;
    }
    tmp[0] = rest+48;
    reverse.append(tmp);


    i ++;
  }


  if (i != 0)
  {
    if (number >= 10)
      number += 7;
    tmp[0] = number + 48;  
    reverse.append(tmp);
  }

  for (int k = 0; k <= i ; k ++)
  {






	int a = reverse.at(i-k);  
  	if (a >= 97 && a <= 122)
  	 	tmp[0] = a-38;
	 else
	    tmp[0] = a;
     result.append(tmp);
  }



  return result;
}
