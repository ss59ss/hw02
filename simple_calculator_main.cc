// simple_calculator_main.cc
#include "simple_calculator.h"
#include <iostream>

using namespace std;

int main(void)
{
	char op;
	double num = (double) 0;
	SimpleCalculator oper (0);
	while (1)
	{
		cin >> num;
		oper.Add(num);
	
		while (1)
		{
			cin >> op;
			if (!(op == '+' || op == '-' || op == '*' || op == '/') && op != '=')
				return 1;
			else if (op == '=')
			{
				cout << oper.value() << endl;
				oper.Subtract(oper.value());
				break;
			}
			else
			{
				cin >> num;
				switch (op)
				{
				case '+' :
					oper.Add(num);
					break;
				case '-' :
					oper.Subtract(num);
					break;
				case '*' :
					oper.Multiply(num);
					break;
				case '/' :
					oper.Divide(num);
					break;
				}
			}
		}
	}
	return 0;
}

