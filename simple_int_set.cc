// simple_int_set.cc
#include "simple_int_set.h"

void sortInt2(int * array , int num)
{
	int tmp = 0;
	for (int start = 1 ; start < num ; start ++)
		for (int i = start ; i < num ; i ++)
			if (array[i] < array[start - 1])
			{
				tmp = array[start - 1];
				array[start - 1] = array[i];
				array [i] = tmp;
			}
}

SimpleIntSet SimpleIntSet::Intersect(const SimpleIntSet& int_set) const{
	int num = 0, intersect[MAX_SIZE];
	SimpleIntSet result;
	
	for (int i = 0; i < size() ; i ++)
	{
		for (int k = 0 ; k < int_set.size() ; k ++)
		{
			if (values()[i] == int_set.values()[k]){
				intersect[num] = values()[i];
				num ++;
			}
		}
	}
	sortInt2(intersect,num);
	result.Set(intersect,num);
	return result; 
}//교

SimpleIntSet SimpleIntSet::Union(const SimpleIntSet& int_set) const{
	int union__[MAX_SIZE],num = 0,check = 1;
	SimpleIntSet result;
	
	for (int i = 0 ; i < size() ; i ++, num++){
		union__[num] = values()[i];
	}
	for (int i = 0 ; i < int_set.size() ; i ++){
		for (int k = 0 ; k < num ; k ++){
			if (union__[k] == int_set.values()[i])
				check = 0;
		}
		if (check){
			union__[num ++] = int_set.values()[i];
		}
		check = 1;
	}
	sortInt2(union__,num);
	result.Set(union__, num);
	return result;
}//합

SimpleIntSet SimpleIntSet::Difference(const SimpleIntSet& int_set) const{
	int num = 0,difference[MAX_SIZE],check = 1;
	SimpleIntSet result;

	for (int i = 0; i < size() ; i ++)
	{
		for (int k = 0 ; k < int_set.size() ; k ++)
		{
			if (values()[i] == int_set.values()[k]){
				check = 0;
				break;
			}
		}
		if (check == 1)
		{
			difference[num] = values()[i];
			num ++;
		}
		check = 1;
	}
	sortInt2(difference,num);
	result.Set(difference, num);
	return result;
}//차

void SimpleIntSet::Set(const int* values, int size){
	for (int i = 0 ; i < size ; i ++)
		values_[i] = values[i];
	size_ = size;
}//클래스에 값 대입
